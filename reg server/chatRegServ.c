/***************************************************************************
* chatRegServ.c: Chat server that accepts client registrations
* History: 
* v1.2.1 2014may26 replaced "unsigned long" with "uint32_t" s.t. 64b processors use 32b for nusers
* v1.2 Revised 2014may21 by Tim Moors for style (don't send structs over net) & compatibility with Visual Studio & bug fixes
* v1.0 created 22 Apr 2007 by Vijay Sivaraman
***************************************************************************/

#include "tim_sockets.h"
#include <stdio.h>
#include <stdint.h> // uint32_t
#include <string.h> // strcmp()
#include <sys/types.h>
#include <errno.h>
#include <assert.h>
#include "skiplist.h"

#define SERV_PORT    31180     /* server UDP port */
#define PASSWORD_LEN 12
#define USERNAME_LEN 14

#define SERV_PASSWD  "3118project" /* password */
#define MAX_MSG_LEN  1200      /* max size of any msg */
#define SERV_AGE_TIME 120      /* seconds after which user is timed out */

#ifndef _WIN32
#  define strncpy_s(A,B,C,D) strncpy(A,C,D)
#  include <time.h> // gettimeofday()
#  include <sys/time.h>
#endif

typedef struct {
	char passwd[PASSWORD_LEN];
	char username[USERNAME_LEN];
	unsigned short port;
} RegMsg_t;

#define MAX_USERS 50

typedef struct User_s {
	char username[USERNAME_LEN];
	unsigned short port;
	unsigned long ipAddr;
	unsigned long timestamp;
} User_t;

typedef struct {
	SkipList_t *userTable; /* list of users */
} ChatRegServ_t;

ChatRegServ_t serv;

/******************************************************************************/
int userTableCmp(void *k1, void *k2)
{
	User_t *u1 = (User_t *)k1;
	User_t *u2 = (User_t *)k2;
	assert(u1 && u2);
	return strcmp(u1->username, u2->username);
}
void userTableFree(void *data)
{
	free(data);
}

#ifdef _WIN32
//!!!should force 32b time to match timeval
// __timeb64 is defined in timeb.h, but VC complains "error C2065: '__timeb64' : undeclared identifier" without this typedef (C version problem?):
typedef struct {
	__time64_t time;
	unsigned short millitm;
	short timezone;
	short dstflag;
} __timeb64;

#  include <sys/types.h>	// ftime()
#  include <sys/timeb.h>	// ftime()

int gettimeofday(struct timeval *tv, struct timezone *tz)
{
	_timeb t;
	errno_t ret = _ftime_s(&t);
	tv->tv_sec = t.time;
	tv->tv_usec = t.millitm * 1000;
	return ret;
}
#endif

/******************************************************************************/
int main()
{
	int sock, e, cliLen, inMsgLen, i;
	struct sockaddr_in servAddr, cliAddr;
	char inMsg[MAX_MSG_LEN], outMsg[MAX_MSG_LEN];
	RegMsg_t regMsg;
	struct timeval timeVal;
	unsigned long curtime;
	User_t dummyUser, *user;
	SkipListNode_t *node;

	/* open socket */
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock == INVALID_SOCKET) {
		printf("socket creation failed errno=%d\n", getSocketError());
		exit(0);
	}
	memset((char *)&servAddr, 0, sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAddr.sin_port = htons(SERV_PORT);
	e = bind(sock, (struct sockaddr *)&servAddr, sizeof(servAddr));
	if (e == SOCKET_ERROR) {
		printf("socket bind failed errno=%d\n", getSocketError());
		exit(0);
	}
	/* create user table */
	serv.userTable = SkipListAlloc(userTableCmp, userTableFree);
	/* run the server main loop */
	while (1) {
		cliLen = sizeof(cliAddr);
		/* wait for msg arrival */
		inMsgLen = recvfrom(sock, inMsg, MAX_MSG_LEN, 0, (struct sockaddr *)&cliAddr, (socklen_t*)&cliLen);

		e = gettimeofday(&timeVal, NULL);
		curtime = timeVal.tv_sec;
		printf("[%ld] Rcvd pkt from %s:%d\n",
			curtime, inet_ntoa(cliAddr.sin_addr),
			ntohs(cliAddr.sin_port));
		/* perform some checks */
		if (inMsgLen == SOCKET_ERROR) {
			printf("Error in recvfrom() errno=%d\n", getSocketError());
			continue;
		}
		if (inMsgLen > MAX_MSG_LEN) {
			printf("Received message too long (size=%d)\n", inMsgLen);
			continue;
		}
		memcpy(regMsg.passwd,   inMsg, PASSWORD_LEN);
		memcpy(regMsg.username, inMsg+PASSWORD_LEN, USERNAME_LEN);
		memcpy(&regMsg.port, inMsg+PASSWORD_LEN+USERNAME_LEN, sizeof(unsigned short));

		regMsg.passwd[PASSWORD_LEN - 1] = 0; // ensure string is null terminated
		/* verify if passwd is correct */
		if (strcmp(regMsg.passwd, SERV_PASSWD) != 0) {
			printf("Passwd mismatch\n");
			continue;
		}
		regMsg.username[USERNAME_LEN - 1] = 0; // ensure string is null terminated

		/* determine if user already in database */
		strncpy_s(dummyUser.username, USERNAME_LEN, regMsg.username, strlen(regMsg.username));
		user = (User_t *)SKIPLIST_NODE_VALUE(
			SkipListGetNode(serv.userTable, &dummyUser));
		if (user == NULL) { /* new user */
			user = (User_t *)malloc(sizeof(User_t)); 
			assert(user);
			memset(user, 0, sizeof(User_t));

			strncpy_s(user->username, USERNAME_LEN, regMsg.username, strlen(regMsg.username));
			user->port = regMsg.port;
			user->ipAddr = cliAddr.sin_addr.s_addr;
			user->timestamp = curtime + SERV_AGE_TIME;
			/* insert user in skiplist */
			SkipListInsert(serv.userTable, user, user, 0);
		}
		else { /* user already exists */
			user->port = regMsg.port;
			user->ipAddr = cliAddr.sin_addr.s_addr;
			user->timestamp = curtime + SERV_AGE_TIME;
		}
		/* remove aged-out entries */
		for (node = SKIPLIST_NODE_FIRST(serv.userTable); node;) {
			user = SKIPLIST_NODE_VALUE(node);
			node = SKIPLIST_NODE_NEXT(node);
			if (user->timestamp < curtime) {
				printf("Aging out user=%s\n", user->username);
				SkipListDelete(serv.userTable, user);
			}
		}

		/* create reply */
		*(uint32_t*)outMsg = htonl(SKIPLIST_NUM_NODES(serv.userTable));
		unsigned offset = sizeof(uint32_t); // where to write this user's data in reply; start after nusers
		for (node = SKIPLIST_NODE_FIRST(serv.userTable), i = 0; node && (i < MAX_USERS); node = SKIPLIST_NODE_NEXT(node), ++i) {
			user = SKIPLIST_NODE_VALUE(node);
			memcpy(outMsg+offset, user->username, USERNAME_LEN);	offset += USERNAME_LEN;
			memcpy(outMsg+offset, &user->port, 2);				offset += 2;
			memcpy(outMsg+offset, &user->ipAddr, 4);				offset += 4; 
		}
		/* send the reply */
		e = sendto(sock, outMsg, offset, 0, (struct sockaddr*)&cliAddr, cliLen);
		if (e == SOCKET_ERROR) {
			printf("sendto() failure errno=%d\n", getSocketError());
			continue;
		}
	} // while (1)

	return 0;
}
