/********************************************************************************
 * TCPserver.c: server receives the file from client using TCP protocol
 * Author: Hassan Habibi
 * History: v0.0: created 6 May 2013
 *          v0.01: Tim Moors: files named by source to allow multiple senders; added compatibility with Visual Studio
 *********************************************************************************/

#include "tim_sockets.h"
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
//#include <sys/types.h>
//#include <signal.h>
//#include <ctype.h>          


#define BACKLOG 5
#define LENGTH 100 


int main (int argc, char *argv[])
{
	int tcpSock; 
	int clientConn; 
	int sin_size; 
	struct sockaddr_in localServer; /* client addr */
	struct sockaddr_in remoteClient; /* server addr */
	char rcvBuf[LENGTH]; // Receiver buffer
	char s[4*4+6]; // ipv4addr:port dotted-decimal 4 bytes @ 3digits+dot per byte, terminal dot space carries ":"; 5dig port # + null term
	int totalByte = 0; // received total bytes 

	if(argc!=2){
	   printf("Usage: %s <tcp-port>\n", argv[0]);	   
	   exit(0);
    }


	/* Get the Socket file descriptor */
	if ((tcpSock = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		fprintf(stderr, "ERROR: Failed to obtain Socket Descriptor. (errno = %d)\n", getSocketError());
		exit(1);
	}
	else 
		printf("[Server] Obtaining socket descriptor successfully.\n");

	/* Fill the client socket address struct */
	memset(&localServer, 0, sizeof(localServer));
	localServer.sin_family = AF_INET; // Protocol Family
	localServer.sin_addr.s_addr = INADDR_ANY; // AutoFill local address
	localServer.sin_port = htons(atoi(argv[1])); // Port number
	
	

	/* Bind a special Port */
	if (bind(tcpSock, (struct sockaddr*)&localServer, sizeof(struct sockaddr)) == SOCKET_ERROR)
	{
		fprintf(stderr, "ERROR: Failed to bind Port. (errno = %d)\n", getSocketError());
		exit(1);
	}
	else 
		printf("[Server] Binded tcp port %s in addr 127.0.0.1 sucessfully.\n", argv[1]);

	/* Listen remote connect/calling */
	if (listen(tcpSock, BACKLOG) == SOCKET_ERROR)
	{
		fprintf(stderr, "ERROR: Failed to listen Port. (errno = %d)\n", getSocketError());
		exit(1);
	}
	else
		printf ("[Server] Listening on port %s ...\n", argv[1]);

	
	while(1)
	{
		sin_size = sizeof(struct sockaddr_in);

		/* Wait a connection, and obtain a new socket file despriptor for single connection */
		if ((clientConn = accept(tcpSock, (struct sockaddr *)&remoteClient, &sin_size)) == SOCKET_ERROR)
		{
			fprintf(stderr, "ERROR: Obtaining new Socket Descriptor. (errno = %d)\n", getSocketError());
			exit(1);
		}
		sprintf(s,"%s-%d",inet_ntoa(remoteClient.sin_addr),ntohs(remoteClient.sin_port));
	       printf("\n\n[Server] got new connection from %s.\n", s);			

		/*Receive File from Client */
	       char* fr_name = s; // "receive.txt";
		FILE *fr = fopen(fr_name, "w");
		if(fr == NULL)
			printf("File %s cannot be opened on server.\n", fr_name);
		else
		{
			memset(rcvBuf, 0, LENGTH); 
			int fr_block_sz = 0;
			while((fr_block_sz = recv(clientConn, rcvBuf, LENGTH, 0)) > 0) 
			{
			    int write_sz = fwrite(rcvBuf, sizeof(char), fr_block_sz, fr);
				if(write_sz < fr_block_sz)
					perror("File write failed on server.\n");
				totalByte += fr_block_sz;
				memset(rcvBuf, 0, LENGTH);
				if (fr_block_sz == 0 || fr_block_sz != LENGTH) 
					break;
			}
			if (fr_block_sz == SOCKET_ERROR)
		    {
		        if (errno == EAGAIN)
	        	{
	                printf("recv() timed out.\n");
	            }
	            else
	            {
					fprintf(stderr, "recv() failed due to errno = %d\n", getSocketError());
					exit(1);
	            }
        	}
			printf("[Server] successfully received the File (total %d Bytes)!\n", totalByte);
			fclose(fr); 
		}

		printf("[Server] connection with Client [IP: %s] / [Port:%d] is closed. \n",inet_ntoa(remoteClient.sin_addr),ntohs(remoteClient.sin_port) );

		close(clientConn);
	}
}
