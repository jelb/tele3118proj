import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Timer;

public class Chat {

	public static void main(String[] args) {
		/*
		 * ******************************** 
		 * ******************************** 
		 * ** Initializing the variables ** 
		 * ********************************
		 * ********************************
		 */
		if (args.length != 4) {
			System.out.println("Proper Client Usage is: java Chat {username} {serverIP} {serverPort}");
			System.out.println("    where: {userName} is your username");
			System.out.println("           {clientPort} your chat port");
			System.out.println("           {serverIP} is the IP address of the user you want to chat with");
			System.out.println("           {serverPort} is the port used by user you want to chat with");
			
			System.out.println("Proper Server Usage is: java Chat {chatport}");
			System.out.println("    where: {chatport} is the chatport you specified in registration");
			System.exit(0);
		}
		
		
		String username = args[0];
		
		int clientChatPort = 0;
		
		try {
			clientChatPort = Integer.parseInt(args[1]);
			if (clientChatPort < 0 || clientChatPort > 65535) {
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			System.out.println("clientChatPort must be an integer between 0 and 65535");
			System.exit(0);
		}
		  
		
		DatagramSocket chatServerSocket ;
		try {
			chatServerSocket = new DatagramSocket(clientChatPort);
			Timer chatServerTimer = new Timer();
			chatServerTimer.schedule(new ChatServer(chatServerSocket, username), 1000, 2000);
		} catch (SocketException e) {
			System.out.println("Could not bind port to server socket");
			e.printStackTrace();
			System.exit(0);
		} 

		InetAddress serverIP = null;

		try {
			serverIP = InetAddress.getByName(args[2]);
		} catch (UnknownHostException e) {
			System.out.println("The serverIP provided was not a valid IP address");
			System.exit(0);
		}
		
		int serverPort = 0;
		try {
			serverPort = Integer.parseInt(args[3]);
			if (serverPort < 0 || serverPort > 65535) {
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			System.out.println("serverPort must be an integer between 0 and 65535");
			System.exit(0);
		}
		
		DatagramSocket chatSocket = null;
		try {
			chatSocket = new DatagramSocket();
		} catch (SocketException e) {
			System.out.println("Could not bind port to client socket");
			e.printStackTrace();
			System.exit(0);
		}
		
		/*
		 * ************************ 
		 * ************************ 
		 * ** Listen for message ** 
		 * ************************ 
		 * ************************
		 */

		while (true) {
			String msg = null;
			
			System.out.print("["+username+"]: ");
			BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
			try {
				msg = inFromUser.readLine();
				
				if (msg.length() > 100) {
					System.out.println("You message must be less than 100 characters");
					continue;
				}
				if (msg.equalsIgnoreCase("exit")) {
					System.out.println("Goodbye");
					System.exit(0);
				}
				if (msg.equals("help")) {
					System.out.println("Commands");
					System.out.println("TCP TRANSFER    - Start transfer to other user");
					System.out.println("exit            - Exit the chat program \n");
					
				}
				if (msg.contains("TCP TRANSFER")) {
					System.out.print("port: ");
					int tcpPortNum = Integer.parseInt(inFromUser.readLine());
					System.out.print("file path: ");
					String filePath = inFromUser.readLine();
					
					boolean isConnected = false;
					Socket socket = null;
					OutputStream outputStream = null;
					
					int i = 1;
					while (!isConnected) {
			            try {
			                socket = new Socket(serverIP, tcpPortNum);
			                outputStream = new DataOutputStream(socket.getOutputStream());
			                isConnected = true;
			            } catch (IOException e) {
			            	System.out.println("Attempt "+i+"... Failed to connect");
			            	try {
								Thread.sleep(500);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
			            	if (i == 5) {
			            		break;
			            	}
			            	i++;
			            }
			        }
					if (!isConnected) {
						System.out.println("Check that TCP Server port is open");
						continue;
					}
				
					ChatTCPClient client = new ChatTCPClient(outputStream, filePath);
			        client.readFile();
			        
			        socket.close();
			        continue;
				}
				
				ChatClient chat = new ChatClient(serverPort, username, serverIP, chatSocket);
				chat.sendMsg(msg);
								
			} catch (IOException e) {
				System.out.println("Could not read input stream");
			}
		}
		
	}

}
