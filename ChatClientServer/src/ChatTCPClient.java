import java.io.*;

public class ChatTCPClient {
    private OutputStream outputStream;
    private String sourceFilePath;
    //private FileEvent fileEvent = null;

    public ChatTCPClient(OutputStream outputStream, String sourceFilePath) {
		this.outputStream = outputStream;
		this.sourceFilePath = sourceFilePath;
    }

    /**
     * Reading file as a stream.
     */
    public void readFile() {
        File file = new File(sourceFilePath);
        byte[] fileBytes = null;
        boolean fileIsRead = true;
        if (file.isFile()) {
            try {
                DataInputStream inputStream = new DataInputStream(new FileInputStream(file));
                long len = (int) file.length();
                fileBytes = new byte[(int) len];
                int read = 0;
                int numRead = 0;
                while (read < fileBytes.length && (numRead = inputStream.read(fileBytes, read,
                        fileBytes.length - read)) >= 0) {
                    read = read + numRead;
                }

                System.out.println("File Read Success");
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("File Read Error");
                fileIsRead = false;
            }
        } else {
            System.out.println("path specified is not pointing to a file");
            fileIsRead = false;
        }
        
        if (fileIsRead) {
        	sendFile(fileBytes);
        }

    }
    
    public void sendFile(byte[] fileBytes) {
        try {
            outputStream.write(fileBytes);
            System.out.println("File has been sent");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

