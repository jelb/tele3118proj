import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.TimerTask;

public class ChatServer extends TimerTask{
	private DatagramSocket chatSocket;
	private String username;

	public ChatServer(DatagramSocket chatSocket, String username) {
		super();
		this.chatSocket = chatSocket;
		this.username = username;
	}
	
	@Override
	public void run() {
		try {
			
			byte[] buf = new byte[215];
			DatagramPacket packet = null;
			
			packet = new DatagramPacket(buf, buf.length);

			try {
				chatSocket.receive(packet);
			} catch (IOException e) {
				System.out.println("Couldnt receive ping msg packet");
			}

			byte[] chatMsg = null;
			chatMsg = packet.getData();
			
			byte[] unameArr = new byte[14];
			byte[] msgArr = new byte[1000];
			
			for (int i = 0; i < chatMsg.length; i++) {
				if (i < 14) {
					unameArr[i] = chatMsg[i];
				} else {
					msgArr[i-14] = chatMsg[i];
				}
			}
			

			System.out.print("\r");
			System.out.println("[" + new String(unameArr) + "]: " + new String(msgArr));
			System.out.print("[" + username + "]: ");

		} catch (Exception e) {
			System.out.println("Chat Registration Response Listener threw an exception");
			e.printStackTrace();
		}
	}
	
}


