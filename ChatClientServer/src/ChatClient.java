import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;

public class ChatClient {
	private int userChatPort;
	private String username;
	private InetAddress userChatAddress;
	private DatagramSocket chatSocket;
	
	public ChatClient(int userChatPort, String username, InetAddress userChatAddress, DatagramSocket chatSocket) {
		super();
		this.userChatPort = userChatPort;
		this.username = username;
		this.userChatAddress = userChatAddress;
		this.chatSocket = chatSocket;
	}
	
	public void sendMsg(String msg) {
		byte[] uname = Arrays.copyOf(username.getBytes(), 14);
		uname[13] = 0;
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(uname);
			outputStream.write(msg.getBytes());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		byte[] chatMsg = outputStream.toByteArray();
		
		try {

			DatagramPacket packet = null;
			packet = new DatagramPacket(chatMsg, chatMsg.length, userChatAddress, userChatPort);
			chatSocket.send(packet);

		} catch (Exception e) {
			System.out.println("Threw an exception while sending chat msg");
		}
	}
	
	
	
}
