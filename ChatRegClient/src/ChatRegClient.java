import java.util.*;
import java.io.*;
import java.net.*;

public class ChatRegClient {

	public static void main(String[] args) throws SocketException {

		/*
		 * ******************************** 
		 * ******************************** 
		 * ** Initializing the variables ** 
		 * ********************************
		 * ********************************
		 */
		if (args.length != 3 && args.length != 4) {
			System.out
					.println("Proper Usage is: java ChatRegClient {userName} {password} {clientChatPort} [{serverIP}]");
			System.out.println("    where: {userName} is the Username you want to use");
			System.out.println("           {password} is the password of the registration server");
			System.out.println(
					"           {clientChatPort} is the UDP port which your chat program sends and receives chat messages");
			System.out.println(
					"           {serverIP} is an optional arg for the registration server IP. Default is 149.171.36.211");
			System.exit(0);
		}

		int clientChatPort = 0;

		String userName = args[0];
		String password = args[1];

		try {
			clientChatPort = Integer.parseInt(args[2]);
			if (clientChatPort < 0 || clientChatPort > 65535) {
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			System.out.println("clientChatPort must be an integer between 0 and 65535");
			System.exit(0);
		}

		InetAddress serverIP = null;

		try {
			if (args.length == 4) {
				serverIP = InetAddress.getByName(args[3]);
			} else {
				serverIP = InetAddress.getByName("149.171.36.211");
			}
		} catch (UnknownHostException e) {
			System.out.println("The serverIP provided was not a valid IP address");
			System.exit(0);
		}

		/*
		 * ***************************** 
		 * ***************************** 
		 * ** Start Chat registration ** 
		 * *****************************
		 * *****************************
		 */

		DatagramSocket regSocket = new DatagramSocket();
		Timer pingTimer = new Timer();
		pingTimer.schedule(new ChatRegPing(clientChatPort, 31180, userName, password, serverIP, regSocket), 0,
				55 * 1000);

		Timer pingListenerTimer = new Timer();
		pingListenerTimer.schedule(new ChatRegPingListener(regSocket), 1000, 2000);
		
		/*
		 * ************************ 
		 * ************************ 
		 * ** Listen for command ** 
		 * ************************ 
		 * ************************
		 */

		while (true) {
			String msg = null;
			
			BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
			try {
				msg = inFromUser.readLine();
				if (msg.equalsIgnoreCase("exit")) {
					System.out.println("Goodbye");
					System.exit(0);
				}
			} catch (IOException e) {
				System.out.println("Could not read input stream");
			}
		}
	}
}
