import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.*;
//import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.TimerTask;

public class ChatRegPing extends TimerTask {

	private int chatPort;
	private int regPort;
	private String username;
	private String password;
	private InetAddress serverAddress;
	private DatagramSocket regSocket;

	public ChatRegPing(int chatPort, int regPort, String username, String password, InetAddress serverAddress,
			DatagramSocket regSocket) {
		super();
		this.chatPort = chatPort;
		this.regPort = regPort;
		this.username = username;
		this.password = password;
		this.serverAddress = serverAddress;
		this.regSocket = regSocket;
	}

	@Override
	public void run() {
		byte[] pwd = Arrays.copyOf(password.getBytes(), 12);
		pwd[11] = 0;
		
		byte[] uname = Arrays.copyOf(username.getBytes(), 14);
		uname[13] = 0;
		
		
		byte[] cPort = new byte[] { (byte) ((new Integer(chatPort).shortValue() & 0xFF00) >> 8), (byte) (new Integer(chatPort).shortValue() & 0x00FF) };
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(pwd);
			outputStream.write(uname);
			outputStream.write(cPort);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		byte[] pingMsg = new byte[28];
		pingMsg = outputStream.toByteArray();
		

		try {

			DatagramPacket packet = null;
			packet = new DatagramPacket(pingMsg, pingMsg.length, serverAddress, regPort);
			regSocket.send(packet);

		} catch (Exception e) {
			System.out.println("Threw an exception during pinging");
		}

	}

}
