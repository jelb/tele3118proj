import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
//import java.util.Arrays;
import java.util.TimerTask;

public class ChatRegPingListener extends TimerTask {
	//private Thread thread;
	private DatagramSocket datagramSocket;

	public ChatRegPingListener(DatagramSocket datagramSocket) {
		super();
		this.datagramSocket = datagramSocket;
	}

	@Override
	public void run() {
		try {
			byte[] buf = new byte[1004];
			DatagramPacket packet = null;
			
			packet = new DatagramPacket(buf, buf.length);

			try {
				datagramSocket.receive(packet);
			} catch (IOException e) {
				System.out.println("Couldnt receive ping msg packet");
			}

			byte[] numUsers = new byte[4];
			byte[] username = new byte[14];
			byte[] userPort = new byte[2];
			byte[] userIP = new byte[4];

			byte[] serverResponse = null;
			serverResponse = packet.getData();
			for (int i = 0; i < 4; i++) {
				numUsers[i] = serverResponse[i];
			}

			System.out.println(new Integer(numUsers[0]) + new Integer(numUsers[1]) + new Integer(numUsers[2])
					+ new Integer(numUsers[3]) + " users:");

			for (int i = 4; i < serverResponse.length; i += 20) {
				for (int index = 0; index < 20; index++) {

					if (index < 14) {
						username[index] = serverResponse[i + index];
					} else if (index < 16) {
						userPort[index - 14] = serverResponse[i + index];
					} else {
						userIP[index - 16] = serverResponse[i + index];
					}

				}
				
				int port = ByteBuffer.wrap(userPort).getShort() & 0xffff;
				if (serverResponse[i] != 0) {
					System.out.println("[" + new String(username) + "] port=" + port + ", IP Address="
							+ new Integer(userIP[0] & 0xff) + "." + new Integer(userIP[1] & 0xff) + "." + new Integer(userIP[2] & 0xff) + "."
							+ new Integer(userIP[3] & 0xff));
				}
			}


		} catch (Exception e) {
			System.out.println("Chat Registration Response Listener threw an exception");
			e.printStackTrace();
		}
	}

}
